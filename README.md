These are OpenMP codes for hyperuniform system of charged colloids with hard-core repulsive Yukawa potential and calculate spectral density (written in OpenMP), written in C.

Ref1:  Salvatore Torquato et. al, PCCP,  2018, 20, 17557-17562


Compile using:

*           gcc -I/home/rajneesh/gsl/include -c filename.c -ffast-math -O3


*           gcc -L/home/rajneesh/gsl/lib filename.o -lgsl -lgslcblas -lm


*           export LD_LIBRARY_PATH=/home/rajneesh/gsl/lib


*           nohup ./a.out &


Change path of the gsl libraies to the path where you have installed gsl in your system.
if gsl is installed in default (root) directories, compile using:


*           gcc filename.c -lgsl -lgslcblas -lm -ffast-math -O3


*           nohup ./a.out &


![DisOrderedSFComparision](/uploads/2380d20e015c1f0b614fdea8603d2d5e/DisOrderedSFComparision.png)

![DisOrderedgrComparision](/uploads/d7075004164074cf6da5f4c798c28851/DisOrderedgrComparision.png)

Partial
 Pair correlation function and spectral density. Solids lines: my data and points: taken from Ref1.

 
Written By::<br/>
Rajneesh Kumar<br/>
Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,<br/>
Bengaluru 560064, India.<br/>
Email: rajneesh[at]jncasr.ac.in<br/>
27 Dec, 2020

